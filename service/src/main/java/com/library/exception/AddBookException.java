package com.library.exception;

public class AddBookException extends RuntimeException {
    public AddBookException(final String message) {
        super(message);
    }

    public AddBookException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AddBookException() {
    }
}
