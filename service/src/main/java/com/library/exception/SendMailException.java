package com.library.exception;

import javax.mail.MessagingException;

public class SendMailException extends RuntimeException {
    public SendMailException(final String message, final MessagingException exception) {
        super(message, exception);
    }
}
