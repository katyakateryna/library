package com.library.converter;

import com.library.model.User;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserDetailsConverter {
    UserDetails convert(User user);
}
