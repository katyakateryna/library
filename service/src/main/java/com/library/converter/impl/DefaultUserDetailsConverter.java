package com.library.converter.impl;

import com.library.converter.UserDetailsConverter;
import com.library.model.Role;
import com.library.model.User;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class DefaultUserDetailsConverter implements UserDetailsConverter {
    @Override
    public UserDetails convert(final User user) {
        final String username = user.getUsername();
        final String password = user.getPassword();
        final String[] roles = getRolesAsArray(user);

        return createUser(username, password, roles);
    }

    private String[] getRolesAsArray(final User user) {
        return user.getUserRoles().stream()
                .map(Role::toString)
                .toArray(String[]::new);
    }

    private org.springframework.security.core.userdetails.User createUser(final String username, final String password, final String... roles) {
        return new org.springframework.security.core.userdetails.User(
                username,
                password,
                AuthorityUtils.createAuthorityList(roles));
    }
}
