package com.library.service.impl;

import com.library.service.MailingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultMailingService implements MailingService {
    private JavaMailSender mailSender;

    @Autowired
    public DefaultMailingService(final JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void sendMails(final List<String> recipients, final String subject, final String text) {
        new Thread(() -> sendMessageInternal(recipients, subject, text)).start();
    }

    private void sendMessageInternal(final List<String> recipients, final String subject, final String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(recipients.toArray(new String[0]));
        message.setSubject(subject);
        message.setText(text);
        mailSender.send(message);
    }
}
