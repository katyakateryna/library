package com.library.service.impl;

import com.library.dao.BookDao;
import com.library.exception.AddBookException;
import com.library.model.Book;
import com.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class DefaultBookService implements BookService {

    private BookDao bookDao;

    @Autowired
    public DefaultBookService(final BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDao.getAllBooks();
    }

    @Override
    public List<Book> findBooksByName(final String name) {
        return bookDao.findBooksByName(name);
    }

    @Override
    public List<Book> findBooksOlderThan(final LocalDate date) {
        return bookDao.findBooksOlderThan(date);
    }

    @Override
    public void addBook(final Book book) {
        try {
            bookDao.addBook(book);
        } catch (RuntimeException e) {
            throw new AddBookException("Cannot add book. Reason: " + e.getMessage());
        }
    }
}
