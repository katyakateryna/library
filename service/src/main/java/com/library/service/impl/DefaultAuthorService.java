package com.library.service.impl;

import com.library.dao.AuthorDao;
import com.library.model.Author;
import com.library.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultAuthorService implements AuthorService {
    private AuthorDao authorDao;

    @Autowired
    public DefaultAuthorService(final AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDao.getAllAuthors();
    }

    @Override
    public Author getAuthorById(final int id) {
        return authorDao.getAuthorById(id);
    }
}
