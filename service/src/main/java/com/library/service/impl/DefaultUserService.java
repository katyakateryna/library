package com.library.service.impl;

import com.library.converter.UserDetailsConverter;
import com.library.dao.UserDao;
import com.library.model.User;
import com.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultUserService implements UserService {
    private UserDao userDao;
    private UserDetailsConverter userDetailsConverter;

    @Autowired
    public DefaultUserService(final UserDao userDao, final UserDetailsConverter userDetailsConverter) {
        this.userDao = userDao;
        this.userDetailsConverter = userDetailsConverter;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) {
        final User user = userDao.getUserByUsername(username);
        return userDetailsConverter.convert(user);
    }

    @Override
    public List<User> getUsersByRoleName(final String roleName) {
        return userDao.getUsersByRoleName(roleName);
    }
}
