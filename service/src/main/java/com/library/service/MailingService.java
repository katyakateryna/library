package com.library.service;

import java.util.List;

public interface MailingService {
    void sendMails(List<String> recipients, String subject, String text);
}
