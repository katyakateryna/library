package com.library.service;

import com.library.model.Book;

import java.time.LocalDate;
import java.util.List;

public interface BookService {
    List<Book> getAllBooks();

    List<Book> findBooksByName(String name);

    List<Book> findBooksOlderThan(LocalDate date);

    void addBook(Book book);
}
