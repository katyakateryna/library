<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<h2>${title}</h2>
<h3>Find book</h3>
<form action="/findBooks" method="POST">
    Name: <input name="bookName" type="text"/>
    <input type="submit" value="Find" />
</form>
<c:forEach items="${books}" var="book">
    <p>${book.name} (ISBN: ${book.isbn}); author: ${book.author.name}</p>
</c:forEach>

<sec:authorize access="hasRole('ROLE_ADMIN')">
    <h3>Add book</h3>
    <form action="/addBook" method="POST">
        Name: <input name="name" type="text"/>
        ISBN: <input name="isbn" type="text"/>
        Author:
        <select name="author">
            <c:forEach items="${authors}" var="author">
                <option value="${author.id}">
                    ${author.name}
                </option>
            </c:forEach>
        </select>
        Date: <input name="date" type="date"/>
        <input type="submit" value="Add" />
    </form>
</sec:authorize>

<form name='logoutForm' action="logout" method='POST'>
    <input name="submit" type="submit" value="Logout" />
</form>