package com.library.form;

import org.hibernate.validator.constraints.NotEmpty;

import java.time.LocalDate;

public class BookForm {
    @NotEmpty
    private String name;
    @NotEmpty
    private String isbn;
    @NotEmpty
    private String author;
    private LocalDate date;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(final LocalDate date) {
        this.date = date;
    }
}
