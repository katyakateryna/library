package com.library.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErrorPageController implements ErrorController {
    private static final String MESSAGE = "message";
    private static final String VIEW_NAME = "info";

    @RequestMapping("/error")
    public ModelAndView error() {
        final ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
        modelAndView.addObject(MESSAGE, "Internal server error");
        return modelAndView;
    }

    @RequestMapping("/forbidden")
    public ModelAndView forbidden() {
        final ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
        modelAndView.addObject(MESSAGE, "Access denied");
        return modelAndView;
    }

    @RequestMapping("/methodNotSupported")
    public ModelAndView methodNotSupported() {
        final ModelAndView modelAndView = new ModelAndView(VIEW_NAME);
        modelAndView.addObject(MESSAGE, "HTTP method not supported");
        return modelAndView;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}