package com.library.controller;

import com.library.exception.AddBookException;
import com.library.form.BookForm;
import com.library.model.Author;
import com.library.model.Book;
import com.library.service.AuthorService;
import com.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
public class BookController {
    private BookService bookService;
    private AuthorService authorService;

    @Autowired
    public BookController(final BookService bookService, final AuthorService authorService) {
        this.bookService = bookService;
        this.authorService = authorService;
    }

    @RequestMapping(value = "/findBooks", method = RequestMethod.POST)
    public ModelAndView findBooks(@RequestParam final String bookName) {
        final ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("title", "Books");
        modelAndView.addObject("books", bookService.findBooksByName(bookName));
        modelAndView.addObject("authors", authorService.getAllAuthors());
        return modelAndView;
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public ModelAndView addBook(@ModelAttribute @Valid final BookForm bookForm, final BindingResult bindingResult) {
        performCheck(bindingResult);

        final Book book = getBookFromRequest(bookForm);
        bookService.addBook(book);

        final ModelAndView modelAndView = new ModelAndView("info");
        modelAndView.addObject("message", "Book " + book.getName() + " added successfully");
        return modelAndView;
    }

    private void performCheck(final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new AddBookException("Cannot add book. Reason: incorrect field values");
        }
    }

    private Book getBookFromRequest(final BookForm bookForm) {
        final Author author = getAuthor(bookForm.getAuthor());
        return createBook(bookForm.getName(), bookForm.getIsbn(), author, bookForm.getDate());
    }

    private Author getAuthor(final String authorId) {
        return authorService.getAuthorById(Integer.valueOf(authorId));
    }

    private Book createBook(final String name, final String isbn, final Author author, final LocalDate date) {
        final Book book = new Book();
        book.setName(name);
        book.setIsbn(isbn);
        book.setAuthor(author);
        book.setStartDate(date);
        return book;
    }

    @ExceptionHandler(AddBookException.class)
    public ModelAndView handleDuplicateISBN(final AddBookException exception) {
        final ModelAndView modelAndView = new ModelAndView("info");
        modelAndView.addObject("message", exception.getMessage());
        return modelAndView;
    }
}
