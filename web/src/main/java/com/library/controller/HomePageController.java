package com.library.controller;

import com.library.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomePageController {
    private AuthorService authorService;

    @Autowired
    public HomePageController(final AuthorService authorService) {
        this.authorService = authorService;
    }

    @RequestMapping("/")
    public ModelAndView homePage() {
        final ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("title", "Books");
        modelAndView.addObject("authors", authorService.getAllAuthors());
        return modelAndView;
    }
}
