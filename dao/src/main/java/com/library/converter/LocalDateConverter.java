package com.library.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

@Converter
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {
    @Override
    public Date convertToDatabaseColumn(final LocalDate date) {
        return Optional.ofNullable(date).map(java.sql.Date::valueOf).orElse(null);
    }

    @Override
    public LocalDate convertToEntityAttribute(final Date date) {
        return Optional.ofNullable(date).map(Date::toLocalDate).orElse(null);
    }
}
