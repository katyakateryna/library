package com.library.dao;

import com.library.model.User;

import java.util.List;

public interface UserDao {
    User getUserByUsername(String username);

    List<User> getUsersByRoleName(String roleName);
}
