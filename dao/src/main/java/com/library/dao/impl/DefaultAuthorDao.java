package com.library.dao.impl;

import com.library.dao.AuthorDao;
import com.library.model.Author;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class DefaultAuthorDao implements AuthorDao {
    private static final String FIND_ALL_AUTHORS_QUERY = "Author.findAll";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Author> getAllAuthors() {
        return entityManager.createNamedQuery(FIND_ALL_AUTHORS_QUERY, Author.class).getResultList();
    }

    @Override
    public Author getAuthorById(final int id) {
        return entityManager.getReference(Author.class, id);
    }

    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
