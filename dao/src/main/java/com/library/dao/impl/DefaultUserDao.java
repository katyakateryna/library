package com.library.dao.impl;

import com.library.dao.UserDao;
import com.library.model.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class DefaultUserDao implements UserDao {
    private static final String FIND_USER_BY_USERNAME_QUERY = "User.findByUsername";
    private static final String FIND_USER_BY_ROLE_QUERY = "User.findByRole";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User getUserByUsername(final String username) {
        return entityManager.createNamedQuery(FIND_USER_BY_USERNAME_QUERY, User.class)
                .setParameter("username", username)
                .getSingleResult();
    }

    @Override
    public List<User> getUsersByRoleName(final String roleName) {
        return entityManager.createNamedQuery(FIND_USER_BY_ROLE_QUERY, User.class)
                .setParameter("role", roleName)
                .getResultList();
    }

    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
