package com.library.dao.impl;

import com.library.dao.BookDao;
import com.library.model.Book;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.List;

@Service
public class DefaultBookDao implements BookDao {
    private static final String FIND_ALL_BOOKS_QUERY = "Book.findAll";
    private static final String FIND_BOOKS_BY_NAME_QUERY = "Book.findByName";
    private static final String FIND_BOOKS_OLDER_THAN_QUERY = "Book.findOlderThan";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Book> getAllBooks() {
        return entityManager.createNamedQuery(FIND_ALL_BOOKS_QUERY, Book.class).getResultList();
    }

    @Override
    public List<Book> findBooksByName(final String name) {
        return entityManager.createNamedQuery(FIND_BOOKS_BY_NAME_QUERY, Book.class)
                .setParameter("name", "%" + name + "%")
                .getResultList();
    }

    @Override
    public List<Book> findBooksOlderThan(final LocalDate date) {
        return entityManager.createNamedQuery(FIND_BOOKS_OLDER_THAN_QUERY, Book.class)
                .setParameter("date", date)
                .getResultList();
    }

    @Override
    @Transactional
    public void addBook(final Book book) {
        entityManager.persist(book);
    }

    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
