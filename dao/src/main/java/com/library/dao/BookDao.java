package com.library.dao;

import com.library.model.Book;

import java.time.LocalDate;
import java.util.List;

public interface BookDao {
    List<Book> getAllBooks();

    List<Book> findBooksByName(String name);

    List<Book> findBooksOlderThan(LocalDate date);

    void addBook(Book book);
}
