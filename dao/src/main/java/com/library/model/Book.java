package com.library.model;

import com.library.converter.LocalDateConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "book")
@NamedQueries({@NamedQuery(name = "Book.findAll", query = "SELECT b FROM Book b"),
        @NamedQuery(name = "Book.findByName", query = "SELECT b FROM Book b WHERE b.name LIKE :name"),
        @NamedQuery(name = "Book.findOlderThan", query = "SELECT b FROM Book b WHERE b.startDate < :date")})

public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(length = 13, nullable = false, unique = true)
    private String isbn;

    @Column(length = 100, nullable = false)
    private String name;

    @Column
    @Convert(converter = LocalDateConverter.class)
    private LocalDate startDate;

    @ManyToOne
    @JoinColumn(name = "author")
    private Author author;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(final Author author) {
        this.author = author;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(final LocalDate startDate) {
        this.startDate = startDate;
    }

    @Override
    public String toString() {
        return name + "(ISBN: " + isbn + "), author " + author.getName();
    }
}
