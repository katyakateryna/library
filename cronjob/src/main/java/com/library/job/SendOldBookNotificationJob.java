package com.library.job;

import com.library.model.Book;
import com.library.model.User;
import com.library.service.BookService;
import com.library.service.MailingService;
import com.library.service.UserService;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SendOldBookNotificationJob extends QuartzJobBean {
    private BookService bookService;
    private UserService userService;
    private MailingService mailingService;

    @Override
    protected void executeInternal(final JobExecutionContext context) {
        final List<Book> oldBooks = bookService.findBooksOlderThan(LocalDate.now().minusYears(5));
        if (!oldBooks.isEmpty()) {
            List<String> recipients = getRecipients();
            final String subject = "Outdated books notification";
            final String text = "Hello! There are some outdated books: " + oldBooks.toString();

            mailingService.sendMails(recipients, subject, text);
        }
    }

    private List<String> getRecipients() {
        final List<User> admins = userService.getUsersByRoleName("ROLE_ADMIN");
        return admins.stream().map(User::getEmail).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Autowired
    public void setBookService(final BookService bookService) {
        this.bookService = bookService;
    }

    @Autowired
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setMailingService(final MailingService mailingService) {
        this.mailingService = mailingService;
    }
}
