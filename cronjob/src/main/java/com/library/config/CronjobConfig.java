package com.library.config;

import com.library.job.SendOldBookNotificationJob;
import com.library.service.BookService;
import com.library.service.MailingService;
import com.library.service.UserService;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

import static org.quartz.CronScheduleBuilder.cronSchedule;

@Configuration
public class CronjobConfig {
    @Autowired
    private BookService bookService;
    @Autowired
    private UserService userService;
    @Autowired
    private MailingService mailingService;
    @Value("${cronjob.cronExpression}")
    private String cronExpression;

    @Bean("sendMailJob")
    public JobDetail jobDetail() {
        return JobBuilder.newJob(SendOldBookNotificationJob.class)
                .setJobData(getJobDataMap())
                .storeDurably()
                .build();
    }

    private JobDataMap getJobDataMap() {
        final HashMap<String, Object> jobDataMap = new HashMap<>();
        jobDataMap.put("bookService", bookService);
        jobDataMap.put("userService", userService);
        jobDataMap.put("mailingService", mailingService);
        return new JobDataMap(jobDataMap);
    }

    @Bean
    public Trigger trigger(@Qualifier("sendMailJob") JobDetail job) {
        return TriggerBuilder.newTrigger().forJob(job)
                .withSchedule(cronSchedule(cronExpression))
                .build();
    }
}
